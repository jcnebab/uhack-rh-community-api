'use strict';

module.exports.handler = (evt, ctx, done) => {
  let { per_page, page } = evt.queryStringParameters
	let response, error
  per_page = parseInt(per_page)
	page = parseInt(page)
  const sqlQuery = require('../util/sqlQuery')
  sqlQuery('select * from supplier_product limit ?', [per_page, page]).then(function(e) {
    if (!e.error) {
        const results_count = e.response.length
        const total_pages = parseInt((results_count + per_page - 1) / per_page)
        response = { 
            statusCode: 200,
            body: JSON.stringify({
                data: e.response,
                total_pages: total_pages,
                results_count:	results_count
            })
        }
    }
    else {
        response = {
            statusCode: 404,
            body: JSON.stringify({message: "error 404"})
        }
    }
    done(error, response)
  })
};
