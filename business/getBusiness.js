'use strict';

module.exports.handler = (evt, ctx, done) => {
  const sqlQuery = require('../util/sqlQuery')
  sqlQuery('select * from business_tbl where id = ?', evt.pathParameters.id).then(function(e) {
    done(e.err, e.response)
  })
};
