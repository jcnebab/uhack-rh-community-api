'use strict';

module.exports.handler = (evt, ctx, done) => {
  const sqlQuery = require('../util/sqlQuery')
  sqlQuery('INSERT INTO business_tbl SET ?', JSON.parse(evt.body)).then(function(e) {
    done(e.err, e.response)
  })
};
