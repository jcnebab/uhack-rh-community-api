'use strict';

module.exports.handler = (evt, ctx, done) => {
  let { per_page, page } = evt.queryStringParameters
  let response, error
  per_page = parseInt(per_page)
	page = parseInt(page)
  const sqlQuery = require('../util/sqlQuery')
  sqlQuery('select business_tbl.name as business_name, business_tbl.id as business_id, business_product.id as product_id, business_product.name as product_name, business_tbl.*, business_product.* from business_tbl right join business_product on business_tbl.id = business_product.business_id limit ?', [per_page, page]).then(function(e) {
    if (!e.error) {
        const results_count = e.response.length
        const total_pages = parseInt((results_count + per_page - 1) / per_page)
        response = { 
            statusCode: 200,
            body: JSON.stringify({
                data: e.response,
                total_pages: total_pages,
                results_count:	results_count
            })
        }
    }
    else {
        response = {
            statusCode: 404,
            body: JSON.stringify({message: "error 404"})
        }
    }
    done(error, response)
  })
};
