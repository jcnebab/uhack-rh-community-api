'use strict'

module.exports = (sqlStatement, param=null) => {
	const mysqlConnection = require('./mysqlConfig')()
	let response, error
    return new Promise((resolve, reject) => {   
        mysqlConnection.query(sqlStatement, param, (err, resp, rows) => {
            if (!err) {
                response = resp
            }
            else {
                error = err
                reject(err)
            }
            mysqlConnection.destroy()
            resolve({response: response, error: error})
        })	
    })    
}
