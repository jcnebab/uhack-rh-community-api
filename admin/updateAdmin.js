'use strict'

module.exports.handler = (evt, ctx, done) => {
    const sqlQuery = require('../util/sqlQuery')
    sqlQuery('UPDATE admin_tbl SET ? WHERE id = ? ', [JSON.parse(evt.body), evt.pathParameters.id]).then(function(e) {
        done(e.err, e.response)
    })
}
