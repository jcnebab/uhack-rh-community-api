'use strict'

module.exports.handler = (evt, ctx, done) => {
    const sqlQuery = require('../util/sqlQuery')
    sqlQuery('UPDATE supplier_tbl set ? WHERE id = ? ', [JSON.parse(evt.body), evt.pathParameters.id]).then(function(e) {
        done(e.err, e.response)
    })
}
