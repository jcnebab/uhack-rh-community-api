'use strict'

module.exports.handler = (evt, ctx, done) => {
    const sqlQuery = require('../util/sqlQuery')
    sqlQuery('insert into admin_tbl set ? ', JSON.parse(evt.body)).then(function(e) {
        done(e.err, e.response)
    })
}
