'use strict'

module.exports.handler = (evt, ctx, done) => {
    const sqlQuery = require('../util/sqlQuery')
    sqlQuery('insert into supplier_tbl set ? ', JSON.parse(evt.body)).then(function(e) {
        done(e.err, e.response)
    })
}
